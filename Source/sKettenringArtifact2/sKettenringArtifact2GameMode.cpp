// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "sKettenringArtifact2GameMode.h"
#include "sKettenringArtifact2HUD.h"
#include "sKettenringArtifact2Character.h"
#include "UObject/ConstructorHelpers.h"

AsKettenringArtifact2GameMode::AsKettenringArtifact2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AsKettenringArtifact2HUD::StaticClass();
}
